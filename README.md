# Spring Boot Flyway

Run this project by this command :

`gradle clean bootRun`

Database migration with [Flyway](https://flywaydb.org/)

Example for blog post: http://zoltanaltfatter.com/2016/03/14/database-migration-with-flyway
```
$ ./gradlew clean build
$ docker-compose up
```

In the logs it is visible that the database changes are applied
```
athletes-service | 2018-09-29 03:34:46.896  INFO 1 --- [ost-startStop-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
athletes-service | 2018-09-29 03:34:46.897  INFO 1 --- [ost-startStop-1] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 2340 ms
athletes-service | 2018-09-29 03:34:47.473  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'characterEncodingFilter' to: [/*]
athletes-service | 2018-09-29 03:34:47.474  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'webMvcMetricsFilter' to: [/*]
athletes-service | 2018-09-29 03:34:47.474  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'hiddenHttpMethodFilter' to: [/*]
athletes-service | 2018-09-29 03:34:47.474  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'httpPutFormContentFilter' to: [/*]
athletes-service | 2018-09-29 03:34:47.475  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'requestContextFilter' to: [/*]
athletes-service | 2018-09-29 03:34:47.476  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'httpTraceFilter' to: [/*]
athletes-service | 2018-09-29 03:34:47.476  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.ServletRegistrationBean  : Servlet dispatcherServlet mapped to [/]
athletes-service | 2018-09-29 03:34:47.548  INFO 1 --- [           main] o.f.core.internal.util.VersionPrinter    : Flyway 4.0 by Boxfuse
athletes-service | 2018-09-29 03:34:47.550  INFO 1 --- [           main] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Starting...
athletes-service | 2018-09-29 03:34:47.634  INFO 1 --- [           main] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Start completed.
athletes-service | 2018-09-29 03:34:47.649  INFO 1 --- [           main] o.f.c.i.dbsupport.DbSupportFactory       : Database: jdbc:postgresql://postgres/mydb (PostgreSQL 9.5)
athletes-service | 2018-09-29 03:34:47.685  INFO 1 --- [           main] o.f.core.internal.command.DbValidate     : Successfully validated 3 migrations (execution time 00:00.009s)
athletes-service | 2018-09-29 03:34:47.709  INFO 1 --- [           main] o.f.c.i.metadatatable.MetaDataTableImpl  : Creating Metadata table: "public"."schema_version"
athletes-service | 2018-09-29 03:34:47.734  INFO 1 --- [           main] o.f.core.internal.command.DbBaseline     : Successfully baselined schema with version: 1
athletes-service | 2018-09-29 03:34:47.752  INFO 1 --- [           main] o.f.core.internal.command.DbMigrate      : Current version of schema "public": 1
athletes-service | 2018-09-29 03:34:47.752  INFO 1 --- [           main] o.f.core.internal.command.DbMigrate      : Migrating schema "public" to version 2 - Add country field to athletes table
athletes-service | 2018-09-29 03:34:47.775  INFO 1 --- [           main] o.f.core.internal.command.DbMigrate      : Migrating schema "public" to version 3 - Create index first name in athletes table
athletes-service | 2018-09-29 03:34:47.798  INFO 1 --- [           main] o.f.core.internal.command.DbMigrate      : Successfully applied 2 migrations to schema "public" (execution time 00:00.056s).
athletes-service | 2018-09-29 03:34:47.891  INFO 1 --- [           main] j.LocalContainerEntityManagerFactoryBean : Building JPA container EntityManagerFactory for persistence unit 'default'
athletes-service | 2018-09-29 03:34:47.907  INFO 1 --- [           main] o.hibernate.jpa.internal.util.LogHelper  : HHH000204: Processing PersistenceUnitInfo [
athletes-service | 	name: default
athletes-service | 	...]
athletes-service | 2018-09-29 03:34:48.016  INFO 1 --- [           main] org.hibernate.Version                    : HHH000412: Hibernate Core {5.2.17.Final}
athletes-service | 2018-09-29 03:34:48.018  INFO 1 --- [           main] org.hibernate.cfg.Environment            : HHH000206: hibernate.properties not found
athletes-service | 2018-09-29 03:34:48.056  INFO 1 --- [           main] o.hibernate.annotations.common.Version   : HCANN000001: Hibernate Commons Annotations {5.0.1.Final}
athletes-service | 2018-09-29 03:34:48.227  INFO 1 --- [           main] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.PostgreSQL9Dialect
athletes-service | 2018-09-29 03:34:48.243  INFO 1 --- [           main] o.h.e.j.e.i.LobCreatorBuilderImpl        : HHH000422: Disabling contextual LOB creation as connection was null
athletes-service | 2018-09-29 03:34:48.245  INFO 1 --- [           main] org.hibernate.type.BasicTypeRegistry     : HHH000270: Type registration [java.util.UUID] overrides previous : org.hibernate.type.UUIDBinaryType@34b9f960

```

Connect to the postgresql instance (using your own DOCKER_HOST IP)

`psql -h 192.168.1.8 -d mydb -U docker`

Show tables :
```
mydb=# \dt
            List of relations
 Schema |      Name      | Type  | Owner
--------+----------------+-------+--------
 public | athletes       | table | docker
 public | schema_version | table | docker
(2 rows)
```

the `schema_version` records the applied changes:
```
mydb=# select * from schema_version;
 installed_rank | version |                description                |   type   |                      script                       |  checksum  | installed_by |        installed_on        | execution_time | success
----------------+---------+-------------------------------------------+----------+---------------------------------------------------+------------+--------------+----------------------------+----------------+---------
              1 | 1       | << Flyway Baseline >>                     | BASELINE | << Flyway Baseline >>                             |            | docker       | 2018-09-29 03:34:47.70096  |              0 | t
              2 | 2       | Add country field to athletes table       | SQL      | V2__Add_country_field_to_athletes_table.sql       | -674532233 | docker       | 2018-09-29 03:34:47.742688 |              5 | t
              3 | 3       | Create index first name in athletes table | SQL      | V3__Create_index_first_name_in_athletes_table.sql | 1143920342 | docker       | 2018-09-29 03:34:47.763059 |              8 | t
(3 rows)
```

Add an Athlete :

`echo '{"country":"Jamaica"}' | http patch http://192.168.1.8:8080/athletes/1`

```
HTTP/1.1 200
Content-Type: application/json;charset=UTF-8
Date: Sat, 29 Sep 2018 04:02:35 GMT
Transfer-Encoding: chunked

{
    "_links": {
        "athlete": {
            "href": "http://192.168.1.8:8080/athletes/1"
        },
        "self": {
            "href": "http://192.168.1.8:8080/athletes/1"
        }
    },
    "country": "Jamaica",
    "firstName": "Usain",
    "lastName": "Bolt"
}
```


Search based on first name:

`http get http://192.168.1.8:8080/athletes/search/findByFirstNameIgnoreCase/\?firstName\=usain`

```
HTTP/1.1 200
Content-Type: application/hal+json;charset=UTF-8
Date: Sat, 29 Sep 2018 04:03:57 GMT
Transfer-Encoding: chunked

{
    "_embedded": {
        "athletes": [
            {
                "_links": {
                    "athlete": {
                        "href": "http://192.168.1.8:8080/athletes/1"
                    },
                    "self": {
                        "href": "http://192.168.1.8:8080/athletes/1"
                    }
                },
                "country": "Jamaica",
                "firstName": "Usain",
                "lastName": "Bolt"
            }
        ]
    },
    "_links": {
        "self": {
            "href": "http://192.168.1.8:8080/athletes/search/findByFirstNameIgnoreCase/?firstName=usain"
        }
    }
}
```

Clean after yourself:

To easily stop containers and remove the containers, networks, volumes and images created by docker-compose up I use

`docker-compose down -v --rmi=all`

I think Flyway is a very useful tool for handling database schema changes especially in a continuous delivery pipeline.