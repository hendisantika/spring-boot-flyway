package com.hendisantika.springbootflyway.repository;

import com.hendisantika.springbootflyway.entity.Athlete;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-flyway
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/08/18
 * Time: 07.43
 * To change this template use File | Settings | File Templates.
 */
//@RepositoryRestResource(collectionResourceRel = "athletes", path = "/athletes")
interface AthleteRepository extends CrudRepository<Athlete, Long> {

    List<Athlete> findByFirstNameIgnoreCase(@Param("firstName") String firstName);

}